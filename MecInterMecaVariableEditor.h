/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECINTERMECAVARIABLEEDITOR_H__
#define __MECINTERMECAVARIABLEEDITOR_H__

#include <MecVariableEditor.h>

/**
\brief	Classe d'édition des variables d'élément « InterMeca ».

Cette classe restreint l'édition de ces variables : le type de variable ne peut être que bool, double, int, string ou uint.
*/
class MecInterMecaVariableEditor : public MecVariableEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Variable	Variable éditée, doit absolument exister lors de la construction (c.à.d. instanciée et différente de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecInterMecaVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecInterMecaVariableEditor();

private:


};

#endif /* __MECINTERMECAVARIABLEEDITOR_H__ */

