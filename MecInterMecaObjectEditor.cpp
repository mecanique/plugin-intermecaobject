/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecInterMecaObjectEditor.h"

MecInterMecaObjectEditor::MecInterMecaObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
pushButtonPeer = new QPushButton(tr("Export peer object"), this);
	connect(pushButtonPeer, SIGNAL(clicked(bool)), SLOT(savePeerObject()));

tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));

layoutGeneral->addWidget(pushButtonPeer);
}

MecInterMecaObjectEditor::~MecInterMecaObjectEditor()
{
}

bool MecInterMecaObjectEditor::canAddVariable() const
{
	return false;
}

bool MecInterMecaObjectEditor::canRemoveChild(MecAbstractElement* const Element) const
{
	if (Element->elementName() == "address" or Element->elementName() == "identifier" or Element->elementName() == "isConnected" or Element->elementName() == "connected" or Element->elementName() == "disconnected") return false;
	else return true;
}

MecAbstractElementEditor* MecInterMecaObjectEditor::newSubEditor(MecAbstractElement *Element)
{
	if (Element->elementRole() == MecAbstractElement::Function)
		{
		MecElementEditor *tempEditor = 0;
		if (Element->elementName() == "address" or Element->elementName() == "identifier" or Element->elementName() == "isConnected") tempEditor = new MecInterMecaSpecialFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
		else tempEditor = new MecInterMecaFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
		tempEditor->childListElementsChanged(Element);
		tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
		connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
		return tempEditor;
		}
	else if (Element->elementRole() == MecAbstractElement::Signal)
		{
		MecElementEditor *tempEditor = 0;
		if (Element->elementName() == "connected" or Element->elementName() == "disconnected") tempEditor = new MecInterMecaSpecialSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
		else tempEditor = new MecInterMecaSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
		tempEditor->childListElementsChanged(Element);
		tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
		connect(Element, SIGNAL(nameChanged(MecAbstractElement*)), SLOT(childNameChanged(MecAbstractElement*)));
		return tempEditor;
		}
else return 0;
}
	
void MecInterMecaObjectEditor::addFunction()
{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Function, "void");
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the function “%1”").arg(tempElement->elementName()));
}
	
void MecInterMecaObjectEditor::addSignal()
{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Signal, "void");
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
}
	
void MecInterMecaObjectEditor::addVariable()
{
return;
}

void MecInterMecaObjectEditor::savePeerObject()
{
QString tempFileName = QFileDialog::getSaveFileName(this, tr("Save the peer object as..."), QDir::homePath(), tr("Mécanique file (*.mec)"), 0, QFileDialog::DontResolveSymlinks);
if (tempFileName.isEmpty()) return;
if (!tempFileName.endsWith(".mec", Qt::CaseInsensitive)) tempFileName.append(".mec");

QFile fileOutput(tempFileName);
if (fileOutput.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
	fileOutput.write(peerObjectFileContent());
		
	fileOutput.close();
	if (fileOutput.error() != QFile::NoError)
		{
		QMessageBox::critical(this, tr("Writing error"), tr("<h3>Writing error</h3><p>") + fileOutput.errorString() + tr("</p>"));
		}
	}
else
	{
	QMessageBox::critical(this, tr("Writing error"), tr("<h3>Writing error</h3><p>The file “") + tempFileName + tr("” could not be opened.</p>"));
	}
}

QByteArray MecInterMecaObjectEditor::peerObjectFileContent()
{
QString tempString("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n\
<!DOCTYPE project PUBLIC \"-//Mecanique//Mecanique 1//EN\" \"http://repository.mecanique.cc/data/dtd/mecanique.1.dtd\">\n\
<project name=\"ExportPeerOf_" + element()->elementName() + "\">\n\
	<title>Export peer object of " + element()->elementName() + "</title>\n\
	<synopsis></synopsis>\n\
	<object name=\"" + element()->elementName() + "\" type=\"InterMeca\">\n\
		<function name=\"address\" type=\"string\">\n\
			<code></code>\n\
		</function>\n\
		<function name=\"identifier\" type=\"string\">\n\
			<code></code>\n\
		</function>\n\
		<function name=\"isConnected\" type=\"bool\">\n\
			<code></code>\n\
		</function>\n\
		<signal name=\"connected\" />\n\
		<signal name=\"disconnected\" />\n");

for (int i=0 ; i < element()->childElements().size() ; i++)
	{
	MecAbstractElement *tempElement = element()->childElements().at(i);
	if (tempElement->elementName() != "address"
		and tempElement->elementName() != "identifier"
		and tempElement->elementName() != "isConnected"
		and tempElement->elementName() != "connected"
		and tempElement->elementName() != "disconnected")
		{
		if (tempElement->elementRole() == MecAbstractElement::Function)
			{
			tempString += "\t\t<signal name=\"" + tempElement->elementName() + "\">\n";
			}
		else if (tempElement->elementRole() == MecAbstractElement::Signal)
			{
			tempString += "\t\t<function name=\"" + tempElement->elementName() + "\" type=\"void\">\n";
			}
		
		
		for (int j=0 ; j < tempElement->childElements().size() ; j++)
			{
			tempString += "\t\t\t<variable name=\"" + tempElement->childElements().at(j)->elementName() + "\" type=\"" + tempElement->childElements().at(j)->elementType() + "\" default=\"\"/>\n";
			}
		
		
		if (tempElement->elementRole() == MecAbstractElement::Function)
			{
			tempString += "\t\t</signal>\n";
			}
		else if (tempElement->elementRole() == MecAbstractElement::Signal)
			{
			tempString += "\t\t</function>\n";
			}
		}
	}

tempString.append("\t</object>\n</project>");

return tempString.toUtf8();
}


