<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>InterMeca</name>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="93"/>
        <source>Not connected.</source>
        <translation>Aucune connexion.</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="104"/>
        <source>Connection to &quot;%1&quot;</source>
        <translation>Connexion à « %1 »</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="106"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="108"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="112"/>
        <source>Address of &quot;%1&quot; (IP or hostname)</source>
        <translation>Adresse de « %1 » (IP ou nom d&apos;hôte)</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="238"/>
        <source>Try connection to &quot;%1&quot;...</source>
        <translation>Tentative de connexion à « %1 »...</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="246"/>
        <source>Connected to &quot;%1&quot;.</source>
        <translation>Connecté à « %1 ».</translation>
    </message>
    <message>
        <location filename="../../src/InterMeca/InterMeca.cpp" line="255"/>
        <source>Disconnected to &quot;%1&quot;.</source>
        <translation>Déconnecté de « %1 ».</translation>
    </message>
</context>
</TS>
