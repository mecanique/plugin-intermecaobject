/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "InterMeca.h"

InterMecaServer::InterMecaServer()
{
	connect(this, SIGNAL(newConnection()), SLOT(receiveConnectSignal()));
	listen(QHostAddress::Any, 42100);
}

InterMecaServer::~InterMecaServer()
{
	while (sockets.size() != 0) delete sockets.takeFirst();
}
		
void InterMecaServer::receiveConnectSignal()
{
	while (hasPendingConnections())
		{
		QTcpSocket *tempSocket = nextPendingConnection();
		connect(tempSocket, SIGNAL(readyRead()), SLOT(readConnectSignal()));
		connect(tempSocket, SIGNAL(disconnected()), SLOT(connectionFinished()));
		sockets.append(tempSocket);
		}
}

void InterMecaServer::readConnectSignal()
{
	for (int i=0 ; i < sockets.size() ; i++)
	{
		if (sockets.at(i)->canReadLine())
		{
			QString tempLine = QString::fromUtf8(sockets.at(i)->readLine());
			
			for (int j=0 ; j < listeners.size() ; j++)
			{
				if (listeners.at(j)->isConnected() == false and tempLine == QString("Mecanique_" + listeners.at(j)->identifier() + "\n"))
				{
					listeners.at(j)->socket = sockets.takeAt(i);
					connect(listeners.at(j)->socket, SIGNAL(readyRead()), listeners.at(j), SLOT(readFunction()));
					connect(listeners.at(j)->socket, SIGNAL(disconnected()), listeners.at(j), SLOT(connectionFinished()));
					listeners.at(j)->sendConnectSignal();
					listeners.at(j)->connectionSuccess();
				}
			}
		}
	}
}

void InterMecaServer::removeInterMeca(QObject *listener)
{
	listeners.removeAt(listeners.indexOf(static_cast<InterMeca*>(listener)));
	
	if (listeners.size() == 0) deleteLater();
}

void InterMecaServer::addInterMeca(InterMeca *listener)
{
	listeners.append(listener);
}

InterMecaServer* InterMecaServer::server()
{
	static InterMecaServer* staticServer = new InterMecaServer();
	return staticServer;
}

InterMeca::InterMeca(QString Name, Project* const Project) : Object(Name, QString("InterMeca"), Project)
{
	m_connected = false;
	socket = 0;
	
	InterMecaServer::server()->addInterMeca(this);
	
	changeStatus(Object::Error);
	changeInfos(tr("Not connected."));
}

InterMeca::~InterMeca()
{
	delete socket;
}

void InterMeca::more()
{
	QDialog *tempDialog = new QDialog();
		tempDialog->setWindowTitle(tr("Connection to \"%1\"").arg(name()));
	QLineEdit *tempAddress = new QLineEdit(tempDialog);
	QPushButton *buttonConnection = new QPushButton(tr("Connection"), tempDialog);
		connect(buttonConnection, SIGNAL(clicked(bool)), tempDialog, SLOT(accept()));
	QPushButton *buttonCancel = new QPushButton(tr("Cancel"), tempDialog);
		connect(buttonCancel, SIGNAL(clicked(bool)), tempDialog, SLOT(reject()));	
	
	QFormLayout *tempLayout = new QFormLayout(tempDialog);
		tempLayout->addRow(tr("Address of \"%1\" (IP or hostname)").arg(name()), tempAddress);
	QHBoxLayout *tempButtonsLayout = new QHBoxLayout();
		tempButtonsLayout->addWidget(buttonConnection);
		tempButtonsLayout->addWidget(buttonCancel);
		tempLayout->addRow(tempButtonsLayout);
	
	int Ok = tempDialog->exec();
	
	if (Ok == QDialog::Accepted)
	{
		tryConnection(tempAddress->text());
	}
	
	delete tempDialog;
}

QVariant InterMeca::settings()
{
	QHash<QString, QVariant> tempSettings;
	
	tempSettings.insert("address", QVariant(address()));
	
	return QVariant(tempSettings);
}

void InterMeca::setSettings(QVariant Settings)
{
	QHash<QString, QVariant> tempSettings(Settings.toHash());
	tryConnection(tempSettings.value("address").toString());
}

QString InterMeca::address() const
{
	if (socket != 0) return (socket->peerName().isEmpty()) ? socket->peerAddress().toString() : socket->peerName();
	else return QString();
}

bool InterMeca::isConnected() const
{
	return m_connected;
}

void InterMeca::prepareFunction(QString Name)
{
	if (!m_connected) return;
	stringToSend = Name;
}

void InterMeca::prepareBool(bool Value)
{
	if (!m_connected) return;
	stringToSend += ";" + (Value) ? "true" : "false";
}

void InterMeca::prepareDouble(double Value)
{
	if (!m_connected) return;
	stringToSend += ";" + QString::number(Value);
}

void InterMeca::prepareInt(int Value)
{
	if (!m_connected) return;
	stringToSend += ";" + QString::number(Value);
}

void InterMeca::prepareString(QString Value)
{
	if (!m_connected) return;
	stringToSend += ";" + QString(Value.toUtf8().toHex());
}

void InterMeca::prepareUInt(unsigned int Value)
{
	if (!m_connected) return;
	stringToSend += ";" + QString(Value);
}

void InterMeca::send()
{
	if (!m_connected) return;
	socket->write(QString(stringToSend + "\n").toUtf8());
	project()->log()->write(name(), "Message send: " + stringToSend);
}

bool InterMeca::takeBool(QString Value)
{
	if (Value == "true") return true;
	else return false;
}

double InterMeca::takeDouble(QString Value)
{
	bool Ok = false;
	double Double = Value.toDouble(&Ok);
	
	if (Ok) return Double;
	else return NAN;
}

int InterMeca::takeInt(QString Value)
{
	return Value.toInt();
}

QString InterMeca::takeString(QString Value)
{
	return QString(QByteArray::fromHex(Value.toUtf8()));
}

unsigned int InterMeca::takeUInt(QString Value)
{
	return Value.toUInt();
}

void InterMeca::tryConnection(QString Address)
{
	m_connected = false;
	delete socket;
	socket = new QTcpSocket(this);
	connect(socket, SIGNAL(connected()), SLOT(sendConnectSignal()));
	connect(socket, SIGNAL(readyRead()), SLOT(readFunction()));
	
	socket->connectToHost(Address, 42100);

	changeStatus(Object::Unknown);
	changeInfos(tr("Try connection to \"%1\"...").arg(Address));
}

void InterMeca::connectionSuccess()
{
	m_connected = true;
	project()->log()->write(name(), QString("Connected to \"%1\".").arg(address()));
	changeStatus(Object::Operational);
	changeInfos(tr("Connected to \"%1\".").arg(address()));
	emit connected();
}

void InterMeca::connectionFinished()
{
	m_connected = false;
	project()->log()->write(name(), QString("Disconnected to \"%1\".").arg(address()));
	changeStatus(Object::Error);
	changeInfos(tr("Disconnected to \"%1\".").arg(address()));
	emit disconnected();
	
	socket->deleteLater();
	socket = 0;
}

void InterMeca::sendConnectSignal()
{
	socket->write(QString("Mecanique_" + identifier() + "\n").toUtf8());
}

void InterMeca::readFunction()
{
	if (isConnected())
		{
		while (socket->canReadLine())
			{
			QByteArray tempArray = socket->readLine();
		
			QStringList tempList = QString::fromUtf8(tempArray).remove(QRegExp("[:space:]")).split(';');
		
			signalReceived(tempList);
			}
		}
	else if (socket->canReadLine() and !isConnected())
		{
		if (QString::fromUtf8(socket->readLine()) == QString("Mecanique_" + identifier() + "\n")) connectionSuccess();
		}
}

