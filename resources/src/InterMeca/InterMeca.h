/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __INTERMECA_H__
#define __INTERMECA_H__

#include <cmath>
#include "../mecanique/Object.h"
#include "../mecanique/string.h"
#include <QtNetwork>

class InterMeca;

class InterMecaServer : private QTcpServer
{
	Q_OBJECT
	
	private:
	///Constructeur.
	InterMecaServer();
	///Destructeur.
	~InterMecaServer();
	
	///Connexions entrantes en attente de validation.
	QList<QTcpSocket*> sockets;
	
	///InterMecas à consulter lors d'une nouvelle connexion entrante.
	QList<InterMeca*> listeners;
	
	private slots:
	/**
	\brief	Gère la réception d'une demande de connexion.

	\note	Est connecté à newConnection().
	*/
	void receiveConnectSignal();
	
	/**
	\brief	Gère l'analyse d'une demande de connexion.
	
	\note	Est connecté à chaque « readyRead() » des sockets en attente.
	*/
	void readConnectSignal();
	
	/**
	\brief	Connecté à chaque signal "destroyed" de chaque InterMeca.
	
	Si \e listener était le dernier objet présent dans \e listeners, le server appelle sa propre méthode "deleteLater".
	*/
	void removeInterMeca(QObject *listener);
	
	public:
	///Ajoute un objet InterMeca à la liste des receveurs potentiels.
	void addInterMeca(InterMeca *listener);
	
	/**
	\brief	Retourne le serveur de connexion pour les objets InterMeca.
	
	Le serveur est initialisé lors du premier appel, le serveur pointé est toujours le même.
	
	\see	staticServer
	*/
	static InterMecaServer *server();
};

class InterMeca : public Object
{
	Q_OBJECT
	
	friend class InterMecaServer;
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Projet	Projet d'appartenance.
	*/
	InterMeca(QString Name, Project* const Project);
	/**
	\brief	Destructeur.
	
	Effectue la déconnexion.
	*/
	~InterMeca();
	
	///Ouvre le sélecteur de port série.
	void more();
	
	///Retourne les paramètres de connexion.
	QVariant settings();
	///Charge les paramètres de connexion.
	void setSettings(QVariant Settings);
	
	///Retourne l'adresse (IP ou nom d'hôte) de l'objet IP.
	QString address() const;
	
	///Retourne l'identifiant de l'objet InterMeca.
	virtual QString identifier() const = 0;
	
	///Indique si la connexion est en cours.
	bool isConnected() const;
	
	signals:
	///Est émis lorsque la connexion est établie.
	void connected();
	///Est émis lorsque la connexion est rompue.
	void disconnected();
	
	protected:
	/**
	\brief	Donne le nom de fonction à envoyer.
	*/
	void prepareFunction(QString Name);
	/**
	\brief	Ajoute un booléen au flux d'envoi.
	*/
	void prepareBool(bool Value);
	/**
	\brief	Ajoute un double au flux d'envoi.
	*/
	void prepareDouble(double Value);
	/**
	\brief	Ajoute un entier au flux d'envoi.
	*/
	void prepareInt(int Value);
	/**
	\brief	Ajoute une chaîne de caractères au flux d'envoi.
	*/
	void prepareString(QString Value);
	/**
	\brief	Ajoute un entier non signé au flux d'envoi.
	*/
	void prepareUInt(unsigned int Value);
	/**
	\brief	Envoi le flux préparé.
	*/
	void send();
	
	/**
	\brief	Transforme Value en valeur booléenne.
	
	\e false est retourné si la conversion échoue.
	*/
	static bool takeBool(QString Value);
	/**
	\brief	Transforme Value en double.
	
	\e NaN est retourné si la conversion échoue.
	*/
	static double takeDouble(QString Value);
	/**
	\brief	Transforme Value en int.
	
	0 est retourné si la conversion échoue.
	*/
	static int takeInt(QString Value);
	/**
	\brief	Transforme Value en chaîne de caractères.
	*/
	static QString takeString(QString Value);
	/**
	\brief	Transforme Value en entier non signé.
	
	0 est retourné si la conversion échoue.
	*/
	static unsigned int takeUInt(QString Value);
	
	
	///Gère l'exécution des signaux reçus.
	virtual void signalReceived(QStringList Params) = 0;
	
	private:
	///Socket TCP de communication.
	QTcpSocket *socket;
	///Données à envoyer.
	QString stringToSend;
	///Indique si la connexion est ouverte.
	bool m_connected;
	
	/**
	\brief	Tente une connexion à un objet InterMeca.
	
	\param	Address	Adresse de l'objet.
	*/
	void tryConnection(QString Address);
	/**
	\brief	Confirme la connexion.
	*/
	void connectionSuccess();
	/**
	\brief	Entérine la perte de connexion.
	*/
	void connectionFinished();
	
	private slots:
	///Envoi la requête de connexion à l'objet IP.
	void sendConnectSignal();
	/**
	\brief	Procédure de gestion de réception de fonction.
	
	Analyse les données reçues par le socket, et appelle « signalReceived() ».
	
	\note	Est également chargé de la confirmation de connexion, avec connectionSuccess().
	*/
	void readFunction();

};

#endif /* __INTERMECA_H__ */

