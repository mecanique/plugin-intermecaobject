/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecInterMecaSpecialFunctionEditor.h"

MecInterMecaSpecialFunctionEditor::MecInterMecaSpecialFunctionEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecFunctionEditor(Function, MainEditor, Parent, F)
{
lineEditName->setEnabled(false);
comboBoxType->setEnabled(false);
codeEditor->setEnabled(false);
tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetCode));
}

MecInterMecaSpecialFunctionEditor::~MecInterMecaSpecialFunctionEditor()
{
}
	
bool MecInterMecaSpecialFunctionEditor::canAddVariable() const
{
	return false;
}


