/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecInterMecaSignalEditor.h"

MecInterMecaSignalEditor::MecInterMecaSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecSignalEditor(Signal, MainEditor, Parent, F)
{
}

MecInterMecaSignalEditor::~MecInterMecaSignalEditor()
{
}

void MecInterMecaSignalEditor::addVariable()
{
bool Ok = false;
QString Type = QInputDialog::getItem(this, tr("New variable"), tr("Type of the new variable:"), QStringList() << "bool" << "double" << "int" << "string" << "uint", 0, false, &Ok);

if (Ok)
	{
	MecAbstractElement *tempElement = mainEditor()->baseElement(MecAbstractElement::Variable, Type);
	tempElement->setParentElement(element());
	mainEditor()->addEditStep(tr("Add the variable “%1”").arg(tempElement->elementName()));
	}
}

MecAbstractElementEditor* MecInterMecaSignalEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() != MecAbstractElement::Variable) return 0;

MecVariableEditor *tempEditor = new MecInterMecaVariableEditor(static_cast<MecAbstractVariable*>(Element), mainEditor(), widgetEditorsVariables);
layoutEditorsVariables->removeItem(spacerItemEditorsVariables);
layoutEditorsVariables->addWidget(tempEditor, 0, Qt::AlignTop);
layoutEditorsVariables->addSpacerItem(spacerItemEditorsVariables);
widgetEditorsVariables->show();
tempEditor->show();

return tempEditor;
}


