/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecInterMecaObjectPlugin.h"

MecInterMecaObjectPlugin::MecInterMecaObjectPlugin() : MecPlugin(QString("intermecaobject"), __INTERMECAOBJECT_VERSION__, MecAbstractElement::Object, QString("InterMeca"), QString("InterMeca object"))
{
}

MecInterMecaObjectPlugin::~MecInterMecaObjectPlugin()
{
}

QString MecInterMecaObjectPlugin::description() const
{
return QString(tr("Plugin to design a Mécanique-to-Mécanique interface using network."));
}

QString MecInterMecaObjectPlugin::copyright() const
{
return QString("Copyright © 2014 – 2015 Quentin VIGNAUD");
}

QString MecInterMecaObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecInterMecaObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecInterMecaObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecInterMecaObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecInterMecaObjectEditor *tempEditor = new MecInterMecaObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecInterMecaObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecInterMecaObjectCompiler *tempCompiler = new MecInterMecaObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}

