/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecInterMecaVariableEditor.h"

MecInterMecaVariableEditor::MecInterMecaVariableEditor(MecAbstractVariable* const Variable, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecVariableEditor(Variable, MainEditor, Parent, F)
{
	comboBoxType->blockSignals(true);
	for (int i=comboBoxType->count() ; i > 0 ; i--) comboBoxType->removeItem(0);
	comboBoxType->addItems(QStringList() << "bool" << "double" << "int" << "string" << "uint");
	comboBoxType->blockSignals(false);
	typeElementChanged(element());
}

MecInterMecaVariableEditor::~MecInterMecaVariableEditor()
{
}

