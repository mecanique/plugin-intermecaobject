/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECINTERMECASPECIALSIGNALEDITOR_H__
#define __MECINTERMECASPECIALSIGNALEDITOR_H__

#include <MecSignalEditor.h>

/**
\brief	Classe d'édition des signaux « connected » et « disconnected » d'objet « InterMeca ».

Cette classe verrouille l'édition de ces signaux.
*/
class MecInterMecaSpecialSignalEditor : public MecSignalEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecInterMecaSpecialSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecInterMecaSpecialSignalEditor();

	///Indique si une variable peut être ajoutée, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e false.
	bool canRemoveChild(MecAbstractElement* const Element) const;


};

#endif /* __MECINTERMECASPECIALSIGNALEDITOR_H__ */

