/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECINTERMECASPECIALFUNCTIONEDITOR_H__
#define __MECINTERMECASPECIALFUNCTIONEDITOR_H__

#include <MecFunctionEditor.h>

/**
\brief	Classe d'édition des fonctions « address », « identifier » et « isConnected » d'objet « InterMeca ».

Cette classe restreint l'édition de ces fonctions :
	- le code n'est pas éditable,
	- les paramètres ne peuvent être ajoutés,
	- le type de retour ne peut être changé.
*/
class MecInterMecaSpecialFunctionEditor : public MecFunctionEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Function	Fonction éditée, doit absolument exister lors de la construction (c.à.d. instanciée et différente de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecInterMecaSpecialFunctionEditor(MecAbstractFunction* const Function, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecInterMecaSpecialFunctionEditor();

	///Indique si une MecVariable peut être ajouté, retourne ici \e false.
	bool canAddVariable() const;

private:


};

#endif /* __MECINTERMECASPECIALFUNCTIONEDITOR_H__ */

