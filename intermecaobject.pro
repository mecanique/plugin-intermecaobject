######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = intermecaobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/intermecaobject.fr.ts

OTHER_FILES += metadata.json

HEADERS += \
	MecInterMecaFunctionEditor.h \
	MecInterMecaSpecialFunctionEditor.h \
	MecInterMecaObjectCompiler.h \
	MecInterMecaObjectEditor.h \
	MecInterMecaObjectPlugin.h \
	MecInterMecaSignalEditor.h \
	MecInterMecaSpecialSignalEditor.h  \
	MecInterMecaVariableEditor.h

SOURCES += \
	MecInterMecaFunctionEditor.cpp \
	MecInterMecaSpecialFunctionEditor.cpp \
	MecInterMecaObjectCompiler.cpp \
	MecInterMecaObjectEditor.cpp \
	MecInterMecaObjectPlugin.cpp \
	MecInterMecaSignalEditor.cpp \
	MecInterMecaSpecialSignalEditor.cpp \
	MecInterMecaVariableEditor.cpp

