/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECINTERMECASIGNALEDITOR_H__
#define __MECINTERMECASIGNALEDITOR_H__

#include <MecSignalEditor.h>
#include "MecInterMecaVariableEditor.h"

/**
\brief	Classe d'édition des signaux d'objet « InterMeca ».

Cette classe restreint l'édition de ces signaux : les paramètres ne peuvent être que de type bool, double, int, string ou uint.
*/
class MecInterMecaSignalEditor : public MecSignalEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecInterMecaSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecInterMecaSignalEditor();

	/**
	\brief	Ajoute un paramètre au signal.

	Demande à l'utilisateur un type de paramètre et ajoute ce dernier.
	Les types de paramètres disponibles sont bool, double, int, string et uint.
	*/
	void addVariable();

	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element(), et être une variable, sinon 0 est retourné.
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

private:


};

#endif /* __MECINTERMECASIGNALEDITOR_H__ */

