/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECINTERMECAOBJECTEDITOR_H__
#define __MECINTERMECAOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include <QByteArray>
#include <QFile>
#include <QFileDialog>
#include "MecInterMecaFunctionEditor.h"
#include "MecInterMecaSpecialFunctionEditor.h"
#include "MecInterMecaSignalEditor.h"
#include "MecInterMecaSpecialSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « InterMeca ».
*/
class MecInterMecaObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecInterMecaObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecInterMecaObjectEditor();

	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e true sauf s'il s'agit des éléments « address », « identifier », « isConnected », « connected » ou « disconnected ».
	bool canRemoveChild(MecAbstractElement* const Element) const;

public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element(), et être une fonction ou un signal, sinon 0 est retourné.
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	/**
	\brief	Ajoute une fonction à l'objet InterMeca.

	La fonction sera soumise aux contraintes des types supportés par le protocole de communication, son type de retour sera nécessairement « void ».
	*/
	void addFunction();

	/**
	\brief	Ajoute un signal à l'objet InterMeca.

	Le signal sera soumis aux contraintes des types supportés par le protocole de communication.
	*/
	void addSignal();

	///Ne fait rien.
	void addVariable();

	/**
	\brief	Enregistre l'objet pair.
	*/
	void savePeerObject();

	/**
	\brief	Retourne le contenu du fichier de l'objet pair.
	*/
	QByteArray peerObjectFileContent();

signals:


private:
	/**
	\brief	Bouton de demande de l'objet pair.
	*/
	QPushButton *pushButtonPeer;


};

#endif /* __MECINTERMECAOBJECTEDITOR_H__ */

