<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecInterMecaFunctionEditor</name>
    <message>
        <location filename="../MecInterMecaFunctionEditor.cpp" line="36"/>
        <source>New variable</source>
        <translation>Nouvelle variable</translation>
    </message>
    <message>
        <location filename="../MecInterMecaFunctionEditor.cpp" line="36"/>
        <source>Type of the new variable:</source>
        <translation>Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../MecInterMecaFunctionEditor.cpp" line="42"/>
        <source>Add the variable “%1”</source>
        <translation>Ajout de la variable « %1 »</translation>
    </message>
</context>
<context>
    <name>MecInterMecaObjectEditor</name>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="24"/>
        <source>Export peer object</source>
        <translation>Exporter l&apos;objet pair</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="76"/>
        <source>Add the function “%1”</source>
        <translation>Ajout de la fonction « %1 »</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="83"/>
        <source>Add the signal “%1”</source>
        <translation>Ajout du signal « %1 »</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="93"/>
        <source>Save the peer object as...</source>
        <translation>Enregistrer l&apos;objet pair sous…</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="93"/>
        <source>Mécanique file (*.mec)</source>
        <translation>Fichier Mécanique (*.mec)</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="105"/>
        <location filename="../MecInterMecaObjectEditor.cpp" line="110"/>
        <source>Writing error</source>
        <translation>Erreur d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="105"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;</source>
        <translation>&lt;h3&gt;Erreur d&apos;écriture&lt;/h3&gt;&lt;p&gt;</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="105"/>
        <source>&lt;/p&gt;</source>
        <translation>&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="110"/>
        <source>&lt;h3&gt;Writing error&lt;/h3&gt;&lt;p&gt;The file “</source>
        <translation>&lt;h3&gt;Erreur d&apos;écriture&lt;/h3&gt;&lt;p&gt;Le fichier « </translation>
    </message>
    <message>
        <location filename="../MecInterMecaObjectEditor.cpp" line="110"/>
        <source>” could not be opened.&lt;/p&gt;</source>
        <translation> » ne peut être ouvert.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>MecInterMecaObjectPlugin</name>
    <message>
        <location filename="../MecInterMecaObjectPlugin.cpp" line="32"/>
        <source>Plugin to design a Mécanique-to-Mécanique interface using network.</source>
        <translation>Greffon de conception d&apos;interface de communication Mécanique-à-Mécanique par le réseau.</translation>
    </message>
</context>
<context>
    <name>MecInterMecaSignalEditor</name>
    <message>
        <location filename="../MecInterMecaSignalEditor.cpp" line="33"/>
        <source>New variable</source>
        <translation>Nouvelle variable</translation>
    </message>
    <message>
        <location filename="../MecInterMecaSignalEditor.cpp" line="33"/>
        <source>Type of the new variable:</source>
        <translation>Type de la nouvelle variable :</translation>
    </message>
    <message>
        <location filename="../MecInterMecaSignalEditor.cpp" line="39"/>
        <source>Add the variable “%1”</source>
        <translation>Ajout de la variable « %1 »</translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
